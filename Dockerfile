FROM adoptopenjdk/openjdk8:jdk8u172-b11-alpine-slim
WORKDIR /opt
EXPOSE 8081
COPY target/tm.war .
ENTRYPOINT ["java", "-jar", "tm.war"]