package ru.t1.ytarasov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @PostMapping("/save")
    void save(@NotNull @WebParam(name = "project") TaskDto task) throws Exception;

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(@NotNull @WebParam(name = "id") final String id) throws Exception;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @WebParam(name = "id") final String id) throws Exception;

}
